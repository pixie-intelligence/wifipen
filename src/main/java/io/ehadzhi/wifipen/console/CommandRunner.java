package io.ehadzhi.wifipen.console;

import ch.qos.logback.core.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/10/17.
 */
@Component
public class CommandRunner {
    private Logger logger = LoggerFactory.getLogger(CommandRunner.class);

    public List<String> run(String command) {
        Process process = exec(command);
        List<String> output = new BufferedReader(new InputStreamReader(process.getInputStream()))
                .lines()
                .collect(Collectors.toList());

        logger.info("Ran - {} - and got output - {}", command, output);
        return output;
    }

    public void run(String command, Integer maxRunTimeSeconds) {
        Process process = exec(command);
        sleep(maxRunTimeSeconds);
        process.destroy();

        logger.info("Ran - {} - for {} seconds", command, maxRunTimeSeconds);
    }

    private Process exec(String command) {
        try {
            return Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sleep(Integer seconds) {
        if (seconds > 0) {
            try {
                TimeUnit.SECONDS.sleep(seconds);
            } catch (InterruptedException e) {
                logger.error("Something went wrong while sleeping. {}", e);
            }
        }
    }
}
