package io.ehadzhi.wifipen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WifipenApplication {

	public static void main(String[] args) {
		SpringApplication.run(WifipenApplication.class, args);
	}
}
