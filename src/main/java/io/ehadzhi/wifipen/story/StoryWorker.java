package io.ehadzhi.wifipen.story;

import io.ehadzhi.wifipen.interfce.WirelessInterface;
import io.ehadzhi.wifipen.interfce.WirelessInterfaceController;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/12/17.
 */
@Component
public class StoryWorker {

    private WirelessInterfaceController controller;

    public void run(){
        List<WirelessInterface> interfaces = controller.enableMonitorMode(controller.interfaces());
    }
}
