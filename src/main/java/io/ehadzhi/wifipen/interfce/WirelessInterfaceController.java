package io.ehadzhi.wifipen.interfce;

import io.ehadzhi.wifipen.console.CommandRunner;
import io.ehadzhi.wifipen.parser.AirmonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/5/17.
 */
@Component
public class WirelessInterfaceController {

    private Logger logger = LoggerFactory.getLogger(WirelessInterfaceController.class);

    private AirmonParser parser;

    private CommandRunner runner;

    public WirelessInterfaceController(AirmonParser parser, CommandRunner runner) {
        this.parser = parser;
        this.runner = runner;
    }

    public List<WirelessInterface> enableMonitorMode(List<WirelessInterface> interfaces) {
        interfaces = interfaces.stream().filter(i -> !i.monitorModeEnabled()).collect(Collectors.toList());
        logger.info("Enabling monitor mode on interfaces: {}", interfaces);
        interfaces.forEach(wirelessInterface ->
                runner.run(String.format("airmon-ng start %s",
                        wirelessInterface.getName())));

        return interfaces();
    }

    public List<WirelessInterface> interfaces() {
        List<WirelessInterface> interfaces = parser.parse(runner.run("airmon-ng"));
        logger.info("Current interfaces: {}", interfaces);
        return interfaces;
    }
}
