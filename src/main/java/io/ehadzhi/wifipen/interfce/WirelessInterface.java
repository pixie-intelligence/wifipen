package io.ehadzhi.wifipen.interfce;

import lombok.Value;

/**
 * Created by ehadzhi on 7/2/17.
 */
@Value
public class WirelessInterface {
    private String physicalDeviceName;
    private String name;

    public boolean monitorModeEnabled(){
        return name.contains("mon");
    }
}
