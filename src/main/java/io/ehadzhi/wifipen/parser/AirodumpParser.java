package io.ehadzhi.wifipen.parser;

import io.ehadzhi.wifipen.scanner.AccessPoint;
import io.ehadzhi.wifipen.scanner.Station;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ehadzhi on 7/13/17.
 */
@Component
public class AirodumpParser {
    private static final int NUM_OF_CLIENT_PROPERTIES = 7;
    private static final String AP_CSV_FIRST_PROPERTY = "BSSID";
    private static final String CLIENT_CSV_FIRST_PROPERTY = "Station MAC";
    private Logger logger = LoggerFactory.getLogger(AirodumpParser.class);

    public List<AccessPoint> parse(List<String> output) {
        logger.info("Attempting to parse: {}", output);
        List<AccessPoint> accessPoints = output.stream()
                .map(this::splitIntoWords)
                .filter(this::isAccessPoint)
                .peek(ap -> logger.info("AP data ={}", ap))
                .map(line -> new AccessPoint(line.get(0), parse(line.get(3)), parse(line.get(4)), line.get(5),
                        line.get(6), parse(line.get(8)), line.get(13), new ArrayList<Station>()))
                .collect(Collectors.toList());

        List<Station> stations = output.stream()
                .map(this::splitIntoWords)
                .filter(this::isClient)
                .peek(client -> logger.info("Client data ={}", client))
                .map(line -> new Station(line.get(0), parse(line.get(3)), line.get(5)))
                .collect(Collectors.toList());

        accessPoints.forEach(accessPoint ->
                accessPoint.getClients().addAll(
                        stations.stream()
                                .filter(station -> station.isClientOf(accessPoint))
                                .collect(Collectors.toList())));

        logger.info("Parsed output into {}", accessPoints);

        return accessPoints;

    }

    private boolean isClient(List<String> words) {
        return words != null && words.size() <= NUM_OF_CLIENT_PROPERTIES
                && words.size() > 0 && !CLIENT_CSV_FIRST_PROPERTY.equals(words.get(0));
    }

    private boolean isAccessPoint(List<String> words) {
        return words != null && words.size() > NUM_OF_CLIENT_PROPERTIES
                && words.size() > 0 && !AP_CSV_FIRST_PROPERTY.equals(words.get(0));
    }

    private List<String> splitIntoWords(String string) {
        string = string.concat(",");
        List<String> words = Arrays.stream(string.split(","))
                .map(String::trim)
                .collect(Collectors.toList());

        logger.debug("{} was split into {} which has a length of {}", string, words, words.size());

        return words;
    }

    private Integer parse(String string) {
        return Integer.parseInt(string);
    }
}
