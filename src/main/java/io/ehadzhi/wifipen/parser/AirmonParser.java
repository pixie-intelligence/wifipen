package io.ehadzhi.wifipen.parser;

import io.ehadzhi.wifipen.interfce.WirelessInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/2/17.
 */
@Component
public class AirmonParser {

    private Logger logger = LoggerFactory.getLogger(AirmonParser.class);

    public List<WirelessInterface> parse(List<String> airmonOutput) {
        return airmonOutput.stream().
                filter(this::isInterface)
                .map(line -> Arrays.asList(line.split("\t")))
                .map(line -> new WirelessInterface(line.get(0), line.get(1)))
                .collect(Collectors.toList());
    }

    private boolean isInterface(String line) {
        return line.startsWith("phy");
    }
}
