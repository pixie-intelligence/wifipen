package io.ehadzhi.wifipen.scanner;

import lombok.Value;

import java.util.List;

/**
 * Created by ehadzhi on 7/13/17.
 */
@Value
public class AccessPoint {
    private String macAddress;
    private Integer channel;
    private Integer speed;
    private String encryption;
    private String cipher;
    private Integer power;
    private String name;
    private List<Station> clients;
}
