package io.ehadzhi.wifipen.scanner;

import lombok.Value;

/**
 * Created by ehadzhi on 7/13/17.
 */
@Value
public class Station {
    private String macAddress;
    private Integer power;
    private String accessPointMacAddress;

    public boolean isClientOf(AccessPoint accessPoint) {
        return accessPointMacAddress.equals(accessPoint.getMacAddress());
    }
}
