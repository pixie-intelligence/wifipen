package io.ehadzhi.wifipen.scanner;

import io.ehadzhi.wifipen.console.CommandRunner;
import io.ehadzhi.wifipen.interfce.WirelessInterface;
import io.ehadzhi.wifipen.interfce.WirelessInterfaceController;
import io.ehadzhi.wifipen.parser.AirodumpParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

/**
 * Created by ehadzhi on 7/13/17.
 */
@Component
public class WirelessNetworkScanner {
    public static final int SCAN_SECONDS = 11;
    private Logger logger = LoggerFactory.getLogger(WirelessNetworkScanner.class);
    private CommandRunner runner;
    private WirelessInterfaceController controller;
    private AirodumpParser parser;

    public WirelessNetworkScanner(CommandRunner runner, WirelessInterfaceController controller, AirodumpParser parser) {
        this.runner = runner;
        this.controller = controller;
        this.parser = parser;
    }

    public List<AccessPoint> scan() {

        List<WirelessInterface> interfaces = controller.enableMonitorMode(controller.interfaces());

        String workInterface = interfaces.stream()
                .findFirst()
                .get().getName();

        logger.info("Starting scan with interfaces {}, work interface {}", interfaces, workInterface);

        String randomOutputFilePrefix = UUID.randomUUID().toString();
        runner.run(String.format("airodump-ng --output-format=csv --write-interval 1 --write %s %s",
                randomOutputFilePrefix, workInterface),
                SCAN_SECONDS);

        List<String> airodumpOutput = runner.run(String.format("cat %s-01.csv", randomOutputFilePrefix));

        List<AccessPoint> accessPoints = parser.parse(airodumpOutput);
        logger.info("Scanned APs: {}", accessPoints);

        return accessPoints;
    }
}
