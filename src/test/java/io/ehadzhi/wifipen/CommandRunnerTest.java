package io.ehadzhi.wifipen;

import io.ehadzhi.wifipen.console.CommandRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/10/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommandRunnerTest {

    @Autowired
    CommandRunner runner;

    @Test
    public void runsCommand() {
        String command = "echo 123";
        String expectedOutput = "123";
        List<String> output = runner.run(command);

        Assert.assertEquals(expectedOutput, output.stream().findAny().get());
    }

    @Test(timeout=1100)
    public void runsCommandAndTerminatesAfterTimeout() {
        String command = "yes 123";
        int timeout = 1;
        runner.run(command, timeout);
    }
}
