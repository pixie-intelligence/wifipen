package io.ehadzhi.wifipen;

import io.ehadzhi.wifipen.scanner.WirelessNetworkScanner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by ehadzhi on 7/10/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AutomatedHackingModeTest {

    @Autowired
    private WirelessNetworkScanner scanner;

    @Test
    public void test() {
        //turn on interfaces
        //scan for networks, arrange them by best signal
        //start hacking one by one giving enough time for each
        //if wps - wps attack
        //if devices associated with network deauth and capture handshake, start dict attack ... and other
        //if no associated start working on next one put this last

        //final result ap-password-wps pin
        try {
            scanner.scan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
