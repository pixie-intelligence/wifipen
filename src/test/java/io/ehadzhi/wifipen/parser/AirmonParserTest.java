package io.ehadzhi.wifipen.parser;

import io.ehadzhi.wifipen.interfce.WirelessInterface;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/2/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AirmonParserTest {
    private Logger logger = LoggerFactory.getLogger(AirmonParserTest.class);

    @Test
    public void test() throws IOException {
        List<String> output = Files.lines(Paths.get("./src/test/java/io/ehadzhi/wifipen/parser/airmon.output"))
                .collect(Collectors.toList());

        AirmonParser parser = new AirmonParser();
        List<WirelessInterface> interfaces = parser.parse(output);

        List<WirelessInterface> expectedInterfaces = Arrays.asList(
                new WirelessInterface("phy1", "wlan0"),
                new WirelessInterface("phy3", "wlan1"));

        Assert.assertArrayEquals(expectedInterfaces.toArray(), interfaces.toArray());
    }
}
