package io.ehadzhi.wifipen.parser;

import io.ehadzhi.wifipen.interfce.WirelessInterface;
import io.ehadzhi.wifipen.scanner.AccessPoint;
import io.ehadzhi.wifipen.scanner.Station;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 7/13/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AirodumpParserTest {

    private Logger logger = LoggerFactory.getLogger(AirodumpParserTest.class);

    @Test
    public void test() throws IOException {
        List<String> output = Files.lines(Paths.get("./src/test/java/io/ehadzhi/wifipen/parser/airodump.output"))
                .collect(Collectors.toList());
        AirodumpParser parser = new AirodumpParser();
        List<AccessPoint> accessPoints = parser.parse(output);

        List<AccessPoint> expectedAccessPoints = Arrays.asList(
                new AccessPoint("98:13:33:46:90:80",
                        2, 54, "WPA",
                        "CCMP", -46, "VIVACOM_NET",
                        Lists.emptyList()),
                new AccessPoint("EA:DE:27:46:77:4A",
                        2, 54, "WPA2",
                        "CCMP", -45, "OpenWrt",
                        Arrays.asList(
                                new Station("DC:D9:16:48:42:88",
                                        -63, "EA:DE:27:46:77:4A")
                        ))
        );
        logger.info("Expected APs ={}", expectedAccessPoints);
        logger.info("Parsed   APs ={}", accessPoints);

        Assert.assertArrayEquals(expectedAccessPoints.toArray(), accessPoints.toArray());
    }
}
